﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem.Sample
{

    public class Global_s_ControlPausa : MonoBehaviour
    {
        [SerializeField]
        private bool runningState = false;
        public GameObject objetoPausar;
        public GameObject objetoReanudar;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ChangeGameRunningState();
            }
        }
        public void ChangeGameRunningState()
        {
            runningState = !runningState;
            if (runningState)
            {
                Pausar();
            }
            else
            {
                Reanudar();
            }
        }

        public void Pausar()
        {
            objetoPausar.SetActive(false);
            objetoReanudar.SetActive(true);
            Debug.Log("juego Pausado");
            //Objetos que dependen del tiempo, como Rigidbodies, FixedUpdate, etc-
            Time.timeScale = 0f;
            //Objetos emisores de audio
            AudioSource[] audios = FindObjectsOfType<AudioSource>();
            foreach (AudioSource audio in audios)
            {
                audio.Pause();
            }
        }

        public void Reanudar()
        {
            objetoPausar.SetActive(true);
            objetoReanudar.SetActive(false);
            Debug.Log("juego corriendo");
            Time.timeScale = 1f;

            AudioSource[] audios = FindObjectsOfType<AudioSource>();
            foreach (AudioSource audio in audios)
            {
                audio.Play();
            }
        }

        public bool IsGamePaused()
        {
            return runningState;
        }

        public void Salir()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
                Application.Quit();
        }
    }
}