﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F3D1A6_Segimiento : MonoBehaviour
{
    public GameObject Cubess;
    public int pasos = 0;
    public int pasos_Totales = 0;
    public bool Left;
    public bool SegirManos;
    public float tiempo;
    public bool cambio;
    // Start is called before the first frame update
    void Start()
    {
        if (SegirManos)
        {
            if (Left)
            {
                Cubess = GameObject.Find("LeftHand");
            }
            else
            {
                Cubess = GameObject.Find("RightHand");
            }
        }
            this.gameObject.transform.position = new Vector3(Cubess.transform.position.x, Cubess.transform.position.y, Cubess.transform.position.z);
            this.gameObject.transform.rotation = new Quaternion(Cubess.transform.rotation.x, Cubess.transform.rotation.y, Cubess.transform.rotation.z, Cubess.transform.rotation.w);
    }

    // Update is called once per frame
    private void Update()
    {

        this.gameObject.transform.position = new Vector3(Cubess.transform.position.x, Cubess.transform.position.y, Cubess.transform.position.z);
        this.gameObject.transform.rotation = new Quaternion(Cubess.transform.rotation.x, Cubess.transform.rotation.y, Cubess.transform.rotation.z, Cubess.transform.rotation.w);

    }
}
