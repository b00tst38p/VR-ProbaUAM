﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F3D1A6_PersonIK : MonoBehaviour
{
    public Transform[] PernasTarget;
    public Transform[] AnimTarget;
    public LayerMask Camadas;
    public float Offset;

    // Update is called once per frame
    void Update()
    {
        for (int x = 0; x < PernasTarget.Length; x++)
        {
            RaycastHit Hit = new RaycastHit();
            if(Physics.Raycast(AnimTarget[x].position + transform.up * 3,-transform.up, out Hit, 3 + Offset, Camadas, QueryTriggerInteraction.Ignore))
            {
                PernasTarget[x].position = Hit.point + transform.up * Offset;
                Debug.DrawLine(AnimTarget[x].position + transform.up * 3, Hit.point, Color.red);
            }
            else
            {
                PernasTarget[x].position = AnimTarget[x].position;
            }
        }    
    }
}
