﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class F3D1A6_Control_Actividad : MonoBehaviour
{
    private F3D1A6_Segimiento segimiento;//Del objeto Left
    public int Pasos_Girar;
    public int Pasos_Terminar;
    public bool Left;
    public bool MirarEnfrente;
    private F3D1A6_Segimiento segimientos1;
    private GameObject Particle_attractor1;
    public GameObject[] BolasDisco;
    public Animator[] ani;
    public GameObject[] Personajes;
    public AudioClip[] Audios;
    public AudioSource[] ControlAudio;
    private GameObject Cubess;
    private GameObject MirarFrente;
    RaycastHit hit;
    private GameObject DDD1;
    private GameObject DDD2;
    private int musica;
    private GameObject luzes1;
    // Start is called before the first frame update
    void Start()
    {
        segimientos1 = GameObject.Find("target Sphere1").GetComponent<F3D1A6_Segimiento>();
        Particle_attractor1 = GameObject.Find("Particle attractor1");
        segimiento = GameObject.Find("Left").GetComponent<F3D1A6_Segimiento>();
        MirarFrente = GameObject.Find("MirarFrente");
        Cubess = GameObject.Find("Cubess");
        luzes1 = GameObject.Find("luzes1");
        DDD1 = this.gameObject;
        if (Left)
        {
            DDD2 = GameObject.Find("DDD (2)");
            DDD2.GetComponent<BoxCollider>().enabled=false;
        }
        else
        {
            DDD2 = GameObject.Find("DDD (1)");
        }
        for (int i = 0; i < Personajes.Length; i++)
        {
            ani[i] = Personajes[i].GetComponent<Animator>(); 
        }
        MirarFrente.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
        MirarFrente.transform.rotation = new Quaternion(Cubess.transform.rotation.x, Camera.main.transform.rotation.y, Cubess.transform.rotation.z, Camera.main.transform.rotation.w);

        Cubess.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - 1, Camera.main.transform.position.z+0.35f);
        Cubess.transform.rotation = new Quaternion(Cubess.transform.rotation.x, Camera.main.transform.rotation.y, Cubess.transform.rotation.z, Camera.main.transform.rotation.w);
        
    }
    private void Awake()
    {
        foreach (AudioSource a in ControlAudio)
        {
            if (!ControlAudio[0].isPlaying)
            {
                musica = Random.Range(0, Audios.Length);
                a.PlayOneShot(Audios[musica]);
            }
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        if (segimiento.tiempo < 3)
        {
            segimiento.tiempo += Time.deltaTime;
        }
        if (segimiento.pasos_Totales != Pasos_Terminar)
        {
            CambioGiro();
            Seguir();
            GiroCabeza();
        }
        else
        {
            segimientos1.Cubess = luzes1;
            Particle_attractor1.transform.position = new Vector3(luzes1.transform.position.x, luzes1.transform.position.y, luzes1.transform.position.z);
            DDD1.SetActive(false);
            DDD2.SetActive(false);
            
        }
    }

    void CambioGiro()
    {
        if (segimiento.pasos == Pasos_Girar)
        {
            if (segimiento.cambio == false)
            {
                segimiento.tiempo = 0;
                segimiento.cambio = true;
            }
            for (int i = 0; i < Personajes.Length; i++)
            {
                Personajes[i].transform.rotation = new Quaternion(Personajes[i].transform.rotation.x, 40, Personajes[i].transform.rotation.z, 40);
            }
            Cubess.GetComponent<BoxCollider>().enabled = false;
            Cubess.transform.rotation = new Quaternion(Cubess.transform.rotation.x, 40, Cubess.transform.rotation.z, 40);
        }

        if (segimiento.pasos == (Pasos_Girar * 2))
        {
            if (segimiento.cambio == true)
            {
                segimiento.tiempo = 0;
                segimiento.cambio = false;
            }
            for (int i = 0; i < Personajes.Length; i++)
            {
                Personajes[i].transform.rotation = new Quaternion(Personajes[i].transform.rotation.x, 60, Personajes[i].transform.rotation.z, 0);
            }
            Cubess.GetComponent<BoxCollider>().enabled = false;
            Cubess.transform.rotation = new Quaternion(Cubess.transform.rotation.x, 60, Cubess.transform.rotation.z, 0);
        }

        if (segimiento.pasos == (Pasos_Girar * 3))
        {
            if (segimiento.cambio == false)
            {
                segimiento.tiempo = 0;
                segimiento.cambio = true;
            }
            for (int i = 0; i < Personajes.Length; i++)
            {
                Personajes[i].transform.rotation = new Quaternion(Personajes[i].transform.rotation.x, 20, Personajes[i].transform.rotation.z, -20);
            }
            Cubess.GetComponent<BoxCollider>().enabled = false;
            Cubess.transform.rotation = new Quaternion(Cubess.transform.rotation.x, 20, Cubess.transform.rotation.z, -20);
        }

        if (segimiento.pasos == (Pasos_Girar * 4))
        {
            if (segimiento.cambio == true)
            {
                segimiento.tiempo = 0;
                segimiento.cambio = false;
            }
            for (int i = 0; i < Personajes.Length; i++)
            {
                Personajes[i].transform.rotation = new Quaternion(Personajes[i].transform.rotation.x, 0, Personajes[i].transform.rotation.z, 0);
            }
            Cubess.GetComponent<BoxCollider>().enabled = false;
            Cubess.transform.rotation = new Quaternion(Cubess.transform.rotation.x, 0, Cubess.transform.rotation.z, 0);
            segimiento.pasos = 0;
        }
    }

    void Seguir()
    {
        MirarFrente.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
        if ((segimiento.pasos >= Pasos_Girar) && (segimiento.pasos < (Pasos_Girar * 2)))
        {
            segimientos1.Cubess = BolasDisco[1];
            Cubess.transform.position = new Vector3(Camera.main.transform.position.x + 0.35f, Camera.main.transform.position.y - 1, Camera.main.transform.position.z);
            if (segimiento.tiempo >= 3)
            {
                Particle_attractor1.transform.position = new Vector3(BolasDisco[1].transform.position.x, BolasDisco[1].transform.position.y, BolasDisco[1].transform.position.z);
            }
        }

        if (segimiento.pasos >= (Pasos_Girar * 2)&& (segimiento.pasos < (Pasos_Girar * 3)))
        {
            segimientos1.Cubess = BolasDisco[2];
            Cubess.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - 1, Camera.main.transform.position.z - 0.35f);
            if (segimiento.tiempo >= 3)
            {
                Particle_attractor1.transform.position = new Vector3(BolasDisco[2].transform.position.x, BolasDisco[2].transform.position.y, BolasDisco[2].transform.position.z);
            }
        }

        if (segimiento.pasos >= (Pasos_Girar * 3))
        {
            segimientos1.Cubess = BolasDisco[3];
            Cubess.transform.position = new Vector3(Camera.main.transform.position.x - 0.35f, Camera.main.transform.position.y - 1, Camera.main.transform.position.z);
            if (segimiento.tiempo >= 3)
            {
                Particle_attractor1.transform.position = new Vector3(BolasDisco[3].transform.position.x, BolasDisco[3].transform.position.y, BolasDisco[3].transform.position.z);
            }
        }

        if (segimiento.pasos >= 0 && (segimiento.pasos < Pasos_Girar))
        {
            segimientos1.Cubess = BolasDisco[0];
            Cubess.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - 1, Camera.main.transform.position.z + 0.35f);
            if (segimiento.tiempo >= 3)
            {
                Particle_attractor1.transform.position = new Vector3(BolasDisco[0].transform.position.x, BolasDisco[0].transform.position.y, BolasDisco[0].transform.position.z);
            }
        }
    }

    void GiroCabeza()
    {
        if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10f))
        {
            if ((hit.transform.name == "Frente1") &&((segimiento.pasos == Pasos_Girar)|| (segimiento.pasos == (Pasos_Girar * 3))))
            {
                Cubess.GetComponent<BoxCollider>().enabled = true;
            }

            if ((hit.transform.name == "Frente") && ((segimiento.pasos == (Pasos_Girar * 2)) || (segimiento.pasos == 0)))
            {
                Cubess.GetComponent<BoxCollider>().enabled = true;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Cubess")
        {
            if (MirarEnfrente)
            {
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10f))
                {
                    if ((hit.transform.name == "Frente1") || (hit.transform.name == "Frente"))
                    {
                        DDD2.GetComponent<BoxCollider>().enabled = true;
                        for (int i = 0; i < ani.Length; i++)
                        {
                            if (Left == true)
                            {
                                ani[i].SetBool("1", true);
                                ani[i].SetBool("2", false);
                            }
                            else
                            {
                                ani[i].SetBool("2", true);
                                ani[i].SetBool("1", false);
                            }
                        }
                        if (Left == true)
                        {
                            segimiento.pasos++;
                            segimiento.pasos_Totales++;
                        }
                        else
                        {
                            segimiento.pasos++;
                            segimiento.pasos_Totales++;
                        }

                        DDD1.SetActive(false);
                        DDD2.SetActive(true);
                    }
                }
            }
            else
            {
                DDD2.GetComponent<BoxCollider>().enabled = true;
                for (int i = 0; i < ani.Length; i++)
                {
                    if (Left == true)
                    {
                        ani[i].SetBool("1", true);
                        ani[i].SetBool("2", false);
                    }
                    else
                    {
                        ani[i].SetBool("2", true);
                        ani[i].SetBool("1", false);
                    }
                }
                if (Left == true)
                {
                    segimiento.pasos++;
                    segimiento.pasos_Totales++;
                }
                else
                {
                    segimiento.pasos++;
                    segimiento.pasos_Totales++;
                }

                DDD1.SetActive(false);
                DDD2.SetActive(true);
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * 10);
    }

}
