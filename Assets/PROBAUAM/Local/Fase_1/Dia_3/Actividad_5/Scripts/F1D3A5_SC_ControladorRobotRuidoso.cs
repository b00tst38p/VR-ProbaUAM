﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class F1D3A5_SC_ControladorRobotRuidoso : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator anim;
    public Vector3 objetivo;
    private Vector3 posInicial;
    private Vector3 posFinal;
    public float avance;
    // Start is called before the first frame update
    void Start()
    {
        posInicial = gameObject.transform.position;
        posFinal = GameObject.Find("posFinal").transform.position;
        agent = gameObject.GetComponent<NavMeshAgent>();
        anim = gameObject.GetComponent<Animator>();
        objetivo = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position == agent.destination)
        {
            anim.SetBool("caminar", false);
            anim.SetBool("Correr", false);
        }
    }

    public void CalcularAvance(int x)
    {

        posInicial = gameObject.transform.position;
        posFinal = GameObject.Find("posFinal").transform.position;


        avance = Vector3.Distance(posInicial, posFinal) / x;
        Debug.Log("calculado: " + avance + " " + Vector3.Distance(posInicial, posFinal));
    }

    public void AvanzaRobot()
    {
        if (gameObject.transform.position != posFinal)
        {
            agent.speed = 0.8f;
            transform.rotation = new Quaternion(0, 180, 0, 1);
            anim.SetBool("caminar", true);
            objetivo -= Vector3.forward * avance;
            agent.destination = objetivo;
        }
    }

    public void RetrocederRobot()
    {
        if (gameObject.transform.position != posFinal)
        {
            if (gameObject.transform.position != posInicial)
            {
                transform.rotation = new Quaternion(0, 0, 0, 1);
                anim.SetBool("Correr", true);
                agent.speed = 1.8f;
                agent.destination = posInicial;
                objetivo = posInicial;
            }
        }
    }

    public void quieto()
    {
        anim.SetBool("Correr", false);
        anim.SetBool("caminar", false);
        anim.SetBool("quieto", true);
    }

    public bool InDestination()
    {
        if (gameObject.transform.position == posFinal)
        {
            return true;
        }
        return false;
    }
}
