﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class F1D3A5_SC_ControladorJuego : MonoBehaviour
{
    public Vector3 ajuste;
    public GameObject arriba;
    public GameObject abajo;
    private Transform player;
    public SteamVR_Action_Boolean accion;
    public F1D3A5_SC_ControladorRobotRuidoso robot;
    public bool arribaB, abajoB, inicioJuego, sentadillaValida;
    public int sentadillas;
    public GameObject obj;
    private int sentadillasNecesarias = 5;
    public int intentos = 20;
    private int sentadillasCorrectas=0;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("VRCamera").GetComponent<Transform>();
        robot = GameObject.Find("Robot").GetComponent<F1D3A5_SC_ControladorRobotRuidoso>();
        arribaB = false;
        abajoB = false;
        sentadillas = 0;
        robot.CalcularAvance(sentadillasNecesarias);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = player.transform.position - (player.transform.forward * 0.08f);

        #region Calibracion
        //calibracion del juego
        if (accion.GetStateUp(SteamVR_Input_Sources.Any))
        {
            if (arriba == null)
            {
                arriba = GameObject.Find("arriba");
                arriba.transform.position = gameObject.transform.position;
            }
            else
            {
                if (abajo == null)
                {
                    abajo = GameObject.Find("abajo");
                    abajo.transform.position = gameObject.transform.position;
                }
                else
                {
                    inicioJuego = true;

                }
            }
        }
        #endregion

        #region Control de sentadillas
        // se valida que este viendo al frente
        // No se que tan necesario sea
        sentadillaValida = Validarmirada();
        if (sentadillasCorrectas< sentadillasNecesarias)
        {            
            //arribaB solo es verdadero cuando se hizo una sentadilla completa
            if (arribaB)
            {
                sentadillas++;
                sentadillasCorrectas++;
                robot.AvanzaRobot();
                abajoB = false;
                arribaB = false;
            }

            //es para meterle complejidad y si retira la mirada de enfrente entonces el robot retrocede
            if (!sentadillaValida)
            {
                obj.SetActive(false);
                robot.RetrocederRobot();
                sentadillasCorrectas = 0;
            }
            else
            {
                obj.SetActive(true);
            }
        }
        #endregion

    }

    public void OnTriggerEnter(Collider other)
    {

        if (inicioJuego)
        {
            if (sentadillaValida)
            {
                if (other.gameObject == abajo)
                {
                    abajoB = true;

                }
                if (other.gameObject == arriba && abajoB)
                {
                    arribaB = true;

                }
            }

        }
    }
    public bool Validarmirada()
    {
        RaycastHit hit;
        Debug.DrawRay(player.position - ajuste, (player.forward - ajuste) * 10, Color.red);

        if (Physics.Raycast(player.position - ajuste, player.forward - ajuste, out hit, 10))
        {
            obj.transform.position = hit.point;

            if (hit.collider.name == "objetoVer")
            {


                return true;
            }


        }

        return false;
    }
}
