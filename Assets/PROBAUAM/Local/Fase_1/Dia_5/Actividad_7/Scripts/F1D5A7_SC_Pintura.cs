﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D5A7_SC_Pintura : MonoBehaviour
{
    public float t = 0;
    public float dificultad = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Renderer>().material.color = new Color(0, 0, 1, t);
    }
    public void pintar()
    {
        t += dificultad;

    }
}
