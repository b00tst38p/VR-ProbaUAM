﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D5A7_SC_ControladorPintado : MonoBehaviour
{
    RaycastHit hit;
    GameObject paredActual;
    F1D5A7_SC_Pintura pared;
    public LineRenderer linea;
    public Transform mano;
    public bool puedePintar;
    public new AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        puedePintar = true;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject paredNueva;
        gameObject.transform.position = mano.position;
        gameObject.transform.rotation = mano.rotation;
        linea.SetPosition(0, gameObject.transform.position);
        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 10))
        {
            linea.SetPosition(1, hit.point);
            paredNueva = hit.collider.gameObject;
            if (paredNueva.name == "cuadro")
            {

                if (paredActual != paredNueva)
                {
                    pared = hit.collider.gameObject.GetComponent<F1D5A7_SC_Pintura>();
                    pared.pintar();
                    paredActual = paredNueva;
                }
            }
        }
        if (puedePintar && !audio.isPlaying)
        {
            audio.Play();
        }
        if (!puedePintar)
        {
            audio.Stop();
        }
    }
}
