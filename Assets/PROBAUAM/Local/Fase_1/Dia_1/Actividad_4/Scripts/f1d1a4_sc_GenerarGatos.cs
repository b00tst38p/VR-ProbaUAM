﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class f1d1a4_sc_GenerarGatos : MonoBehaviour
{
    public GameObject gato;
    public GameObject spawn;
    public GameObject[] catToCopy;
    public int contadorGatos = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject g = GameObject.Find("cat");
        Destroy(g);

        spawn = GameObject.Find("Spawn");
        nuevo_Gato();
    }

    public void nuevo_Gato()
    {
        if (this.enabled == false)
            return;
        int rand = Random.Range(0, 3);
        if (contadorGatos == 0)
            rand = 0;
        //Debug.Log("nuevo gato " + rand);
        gato = Instantiate(catToCopy[rand], spawn.transform.position, spawn.transform.rotation);
        GetComponent<f1d1a4_s_Puntero>().gato = gato;
        f1d1a4_s_ControlGato control = gato.GetComponent<f1d1a4_s_ControlGato>();
        control.puntero = GetComponent<f1d1a4_s_Puntero>().puntero;
        control.limite = GameObject.Find("Limite");
        control.player = GameObject.Find("Global_s_Player");
        control.animacion = gato.GetComponent<Animator>();
        control.controlEscena = GameObject.Find("ControlEscena");
        control.gatoID = contadorGatos;
        contadorGatos++;
    }
}
