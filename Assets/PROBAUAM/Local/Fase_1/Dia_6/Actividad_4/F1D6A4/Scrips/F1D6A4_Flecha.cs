﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D6A4_Flecha : MonoBehaviour
{

    private float speed = 0.05f;
    private Transform Punto_Enfrente;//aqui es a donde debe de llegar la flecha

    //private Vector3 velocity;
    private Vector3 pos_punto;
    private float tiempoActual = 0;


    void Start()
    {
        Punto_Enfrente = GameObject.Find("punto_instancia").transform;
        pos_punto = Punto_Enfrente.position;
    }

    // Update is called once per frame
    void Update()
    {
        tiempoActual += Time.deltaTime;
        
        if (tiempoActual<2)
        {
            transform.position = Vector3.Lerp(transform.position, pos_punto, speed);
            //transform.position = Vector3.SmoothDamp(transform.position, pos_punto, ref velocity, speed);
            transform.LookAt(pos_punto);
        }
        //Destroy(this.gameObject, 0.4f);
    }



}
