﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Playables;

public class F1D6A4_Control : MonoBehaviour
{
    [Header("Parametros colocar diana")]
    private GameObject ReferenciaEnfrente;

    [Header("Parametros del apuntador")]
    private GameObject PuntoArriba;
    private GameObject PuntoEnfrente;


    [Header("Parametros de ballesta")]
    public GameObject flecha_Lanzada;
    public GameObject Prefab_ballesta;
    private GameObject Ballesta;
    private GameObject Position_Ballesta;
    private GameObject ApuntadorBallesta;
    private GameObject FlechasLanzadas;
    public GameObject ObjFlechasLanzadas;

    [Header("Parametros diana")]
    public GameObject DianaArriba;
    public GameObject DianaEnfrente;

    //Parametros de comparación
    private bool comenzo = false;
    private bool comenzoFlechas = false;
    private bool OjosCerrados = false;
    private bool Turno = false;
    private float range=5;
    private RaycastHit hit;
    private float t;
    private float color;
    private int cont=0;

    [Header("Parametros del juego")]
    private GameObject VR;
    public float tiempoEspera = 5;
    private float tiempoActual = 0;
    private float tiempoMaximo = 1;
    public int FlechasALanzar = 10;
    private int fle = 0;
    private GameObject imagen;
    private PlayableDirector playableDirector;
    private PlayableDirector playableDirector2;
    public GameObject Cabeza;
    // Start is called before the first frame update
    void Start()
    {
        ReferenciaEnfrente = GameObject.Find("ReferenciaEnfrente");
        Position_Ballesta = GameObject.Find("Pos_Ballesta");
        VR = GameObject.Find("VR");
        VR.transform.position = Camera.main.transform.position;
        VR.transform.rotation = Camera.main.transform.rotation;
        ReferenciaEnfrente.transform.SetParent(Camera.main.transform);
        Position_Ballesta.transform.SetParent(Camera.main.transform);
        FlechasLanzadas = GameObject.Find("FlechasLanzadas");
        imagen = GameObject.Find("Image");
        playableDirector = this.GetComponent<PlayableDirector>();
        playableDirector2 = GameObject.Find("AnimCerrarOjos").GetComponent<PlayableDirector>();
        Cabeza = GameObject.Find("Head");
    }

    // Update is called once per frame
    void Update()
    {
        if (comenzo)
        {
            revisamosdiana();
        }
        if (/*accionGatillo.GetLastStateDown(SteamVR_Input_Sources.Any) == true */ Input.GetKeyDown(KeyCode.P) && comenzo == false)
        {
            comenzo = true;
            generarDianaArriba();
            generarDianaEnfrente();
            generarBallesta();
            comenzoFlechas = true;
        }
        if (comenzoFlechas == true)
        {
            tiempoEspera -= Time.deltaTime;
        }
        if (fle == 10 && cont==1)
        {
            playableDirector2.Play();
        }
        Physics.Raycast(Cabeza.transform.position, Cabeza.transform.up, out hit, range);
        apuntado();
        Ballesta.transform.LookAt(PuntoEnfrente.transform.position);
        color = Mathf.Lerp(color, t, Time.deltaTime * 3f);
        imagen.GetComponent<Image>().color = new Color(0f, 0f, 0f, color);
    }
    void revisamosdiana()
    {
        if (Physics.Raycast(Cabeza.transform.position, Cabeza.transform.up, out hit, range))
        {

            if (hit.collider.name == "Target" || hit.collider.name == "Amarillo" || hit.collider.name == "Rojo" || hit.collider.name == "Azul" || hit.collider.name == "Negro" || hit.collider.name == "Blanco")
            {
                
                tiempoActual += Time.deltaTime;

                

                if (tiempoEspera <= 0)
                {
                    if (cont == 1)
                    {
                        playableDirector.Pause();
                    }
                    comenzoFlechas = false;
                    if (tiempoActual >= tiempoMaximo)
                    {
                        if (cont == 0)
                        {
                            playableDirector.Play();
                        }
                        tiempoActual = 0;
                        if (fle < FlechasALanzar)
                        {
                            flechaLanzada();
                        }
                        else
                        {
                            if (Turno == false)
                            {
                                GameObject Diana = GameObject.Find("F1D6A4_DianaArriba(Clone)");
                                Diana.transform.position = new Vector3
                                        (Camera.main.transform.position.x, Camera.main.transform.position.y + 4, Camera.main.transform.position.z);
                                Cabeza = Camera.main.gameObject;
                                Destroy(FlechasLanzadas);
                                FlechasLanzadas = Instantiate(ObjFlechasLanzadas);
                                FlechasLanzadas.name = "FlechasLanzadas";
                                tiempoEspera = 5;
                                fle = 0;
                                comenzoFlechas = true;
                                cont++;
                                Turno = true;
                            }
                            else
                            {
                                if (cont >= 2)
                                {
                                    if (OjosCerrados)
                                    {
                                        OjosCerrados = false;
                                    }
                                    else
                                    {
                                        OjosCerrados = true;
                                    }
                                    t = 1f;
                                    Ojos();
                                }
                                cont++;
                                
                            }
                            
                        }

                        if (hit.collider.name == "Amarillo")
                        {

                            //Puntaje += 5;

                        }

                        if (hit.collider.name == "Rojo")
                        {
                            //Puntaje += 4;


                        }

                        if (hit.collider.name == "Azul")
                        {
                            //Puntaje += 3;


                        }
                        if (hit.collider.name == "Negro")
                        {
                            //Puntaje += 2;


                        }
                        if (hit.collider.name == "Blanco")
                        {
                            //Puntaje += 1;


                        }
                    }


                }

            }

        }
    }
    void Ojos()
    {
        if (OjosCerrados)
        {
            Destroy(FlechasLanzadas);
            FlechasLanzadas = Instantiate(ObjFlechasLanzadas);
            FlechasLanzadas.name = "FlechasLanzadas";
            tiempoEspera = 5;
            fle = 0;
            comenzoFlechas = true;
        }
        else
        {
            t = 0f;
            comenzo = false;
        }
    }
    void apuntado()
    {
        PuntoArriba.transform.position = hit.point;
       
        PuntoEnfrente.transform.localPosition = PuntoArriba.transform.localPosition;
    }
    void generarDianaArriba()
    {
        Instantiate(DianaArriba, new Vector3
            (Cabeza.transform.position.x, Cabeza.transform.position.y + 4, Cabeza.transform.position.z),
            Quaternion.Euler(90, Cabeza.transform.rotation.eulerAngles.y + 180, 0));
        
        /*Instantiate(DianaArriba, new Vector3
            (Camera.main.transform.position.x, Camera.main.transform.position.y + 4, Camera.main.transform.position.z),
            Quaternion.Euler(90, Camera.main.transform.rotation.eulerAngles.y + 180, 0));*/
            
            PuntoArriba = GameObject.Find("Referencia");
    }
    void generarDianaEnfrente()
    {
        DianaEnfrente.transform.LookAt(ReferenciaEnfrente.transform.position);
        Instantiate(DianaEnfrente, new Vector3
        (ReferenciaEnfrente.transform.position.x, Camera.main.transform.position.y, ReferenciaEnfrente.transform.position.z),
        Quaternion.Euler(ReferenciaEnfrente.transform.rotation.eulerAngles.x, ReferenciaEnfrente.transform.rotation.eulerAngles.y-181, ReferenciaEnfrente.transform.rotation.eulerAngles.z)
        ); 
        PuntoEnfrente = GameObject.Find("punto_instancia");

    }
    void generarBallesta()
    {
        Instantiate(Prefab_ballesta, new Vector3
            (Position_Ballesta.transform.position.x, Camera.main.transform.position.y - 0.3f, Position_Ballesta.transform.position.z),
            Quaternion.identity
            );
        Ballesta = GameObject.Find("F1D6A4_crossbow(Clone)");
        ApuntadorBallesta = GameObject.Find("Punto");
    }
    void flechaLanzada()
    {
        Instantiate(flecha_Lanzada, ApuntadorBallesta.transform.position, PuntoEnfrente.transform.rotation).transform.parent = FlechasLanzadas.transform;
        fle += 1;
    }
    private void OnDrawGizmos()
    {
        if (Turno)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(Camera.main.transform.position, Camera.main.transform.up * range);
        }
        else
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(Cabeza.transform.position, Cabeza.transform.up * range);
        }
    }
}
