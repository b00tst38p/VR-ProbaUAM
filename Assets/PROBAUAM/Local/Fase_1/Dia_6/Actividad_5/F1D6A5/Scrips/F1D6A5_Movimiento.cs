﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F1D6A5_Movimiento : MonoBehaviour
{
    public bool tocandoSuelo = true;
    private AudioSource audioStun;

    public GameObject prefab_cubito;
    private GameObject puntillas;
    public float tiempo;
    public Animator sheep;
    private GameObject spawnTronco;
    private GameObject spawnPasto;
    private int saltos = 0;
    private bool comenzo = false;

    [Header("Aqui son los vidas")]
    private float temporizadorInicio = 4;

    public int Puntos = 10;

    public int contadorVidas = 3;
    public GameObject obeja;
    private bool murio = false;
    private int controlVidas = 0;


    void Start()
    {
        audioStun = GetComponent<AudioSource>();
        spawnPasto = GameObject.Find("SpawPasto");
        spawnTronco = GameObject.Find("SpawTronco");

    }


    void Update()
    {
        if (murio == true)
        {
            temporizadorInicio -= Time.deltaTime;
            if (temporizadorInicio <= 0)
            {
                temporizadorInicio = 4;
                murio = false;
                spawnPasto.SetActive(true);
                spawnTronco.SetActive(true);
                obeja.transform.rotation = Quaternion.Euler(0, 90, 0);
                contadorVidas = 3;
            }

        }

        if (contadorVidas == 0)
        {
            obeja.transform.rotation = Quaternion.Euler(0, 90, -90);
            murio = true;
        }
        if (comenzo == false)
        {
            tiempo += Time.deltaTime;
        }
        if (tiempo >= 2)
        {
            if (puntillas == null)
            {
                puntillas = Instantiate(prefab_cubito, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y + 0.145f, Camera.main.transform.position.z),
                Quaternion.identity);
                puntillas.name = "Cubito";
                sheep.SetBool("Caminar", true);
                comenzo = true;
            }
        }

        if (saltos == 500)
        {
            spawnPasto.SetActive(false);
            spawnTronco.SetActive(false);
            //textMensaje.GetComponent<Text>().text = "Completado";
        }
        if (controlVidas == 5)
        {
            controlVidas = 0;
            if (contadorVidas != 3)
            {
                contadorVidas++;
            }

        }
        if (contadorVidas == 0)
        {
            spawnPasto.SetActive(false);
            spawnTronco.SetActive(false);
        }

    }

    public void saltar()
    {
        if (tocandoSuelo == true)
        {
            //sheepRigi.AddForce(0, 260, 0);
            sheep.SetBool("Jump", true);
        }



    }
    public void caminar()
    {
        sheep.SetBool("Stun", false);
    }
    public void stun()
    {
        sheep.SetBool("Stun", true);

        if (contadorVidas >= 0)
        {
            audioStun.Play();
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tronco"))
        {
            stun();
        }

        if (other.name == "Suelo2")
        {
            Debug.Log("Esta tocando el suelo");
            tocandoSuelo = true;
        }

        if (other.name == "Salto")
        {
            saltos++;
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Tronco"))
        {
            caminar();
        }
        if (other.name == "Suelo2")
        {
            tocandoSuelo = false;
            sheep.SetBool("Jump", false);
        }
    }



}
