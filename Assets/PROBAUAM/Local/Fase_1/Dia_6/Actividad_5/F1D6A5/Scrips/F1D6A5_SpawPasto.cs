﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D6A5_SpawPasto : MonoBehaviour
{
    public float tRandom = 5;
    private float tiempo_g = 0;
    public GameObject pasto;
    private float tiempo;
    public GameObject SpawTronco;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        tiempo_g += Time.deltaTime;
        tiempo += Time.deltaTime;
        if (tiempo >= 2)
        {
            if (tiempo_g >= tRandom)
            {
                tRandom = Random.Range(4, 8);
                Instantiate(pasto, SpawTronco.transform.position, Quaternion.Euler(90, 0, 0));
                tiempo_g = 0;
            }

        }
    }
}
