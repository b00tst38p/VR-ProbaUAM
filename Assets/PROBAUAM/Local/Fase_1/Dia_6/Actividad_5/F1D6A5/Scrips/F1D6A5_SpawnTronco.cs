﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D6A5_SpawnTronco : MonoBehaviour
{
    public GameObject troncos;
    private float tiempo_generar = 0;
    private float tiempo;
    public GameObject SpawTronco;
    public float tiempoRandom = 2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        troncos.SetActive(true);
        tiempo_generar += Time.deltaTime;
        tiempo += Time.deltaTime;
        if (tiempo >= 2)
        {
            if (tiempo_generar >= tiempoRandom)
            {
                tiempoRandom = Random.Range(2, 5);
                Instantiate(troncos, SpawTronco.transform.position, Quaternion.Euler(90, 0, 0));
                tiempo_generar = 0;
            }

        }
    }
}
