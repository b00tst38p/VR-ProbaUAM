﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class F1D4A7_SC_ControladorPiñata : MonoBehaviour
{
    public NavMeshAgent piñata, niño;
    public bool derecha;
    public bool izquierda;
    public Transform pos1, pos2;
    public Transform pos1N, pos2N;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (piñata.gameObject.transform.position == piñata.destination)
        {
            niño.destination = piñata.gameObject.transform.position + (new Vector3(0, 0, -1.032f));
            if (derecha)
                anim.SetBool("derecha", true);
            if (izquierda)
                anim.SetBool("izquierda", true);
        }
        if (niño.gameObject.transform.position == niño.destination)
        {
            anim.SetBool("derecha", false);
            anim.SetBool("izquierda", false);
        }
    }
    public void moverPiñata(int tipo)
    {

        if (tipo == 0)
        {
            piñata.destination = pos1.position;
            izquierda = false;
            derecha = true;
        }
        if (tipo == 1)
        {
            piñata.destination = pos2.position;
            derecha = false;
            izquierda = true;
        }
    }
}
