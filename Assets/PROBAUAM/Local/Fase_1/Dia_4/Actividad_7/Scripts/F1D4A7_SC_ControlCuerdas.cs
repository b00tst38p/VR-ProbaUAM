﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class F1D4A7_SC_ControlCuerdas : MonoBehaviour
{
    public F1D4A7_SC_ControladorPiñata piñata;
    public GameObject objetivo;
    public Transform mano;
    LineRenderer cuerda;
    public Vector3 vector = new Vector3(0, 0, 0.5f);
    public SteamVR_Action_Boolean accion;
    public int tipo;
    // Start is called before the first frame update
    void Start()
    {
        cuerda = gameObject.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (accion.GetStateUp(SteamVR_Input_Sources.Any))
        {
            objetivo.transform.position = gameObject.transform.position;

        }
        gameObject.transform.position = mano.transform.position + vector;
        cuerda.SetPosition(1, mano.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.name.Equals(objetivo.name))
        {
            Debug.Log("tipo");
            piñata.moverPiñata(tipo);
        }
    }
}
