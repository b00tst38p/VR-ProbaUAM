﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_CrearPuntosPie : MonoBehaviour
{
    public GameObject CrearPunto;//Crea el cubo que va a tocar
    public GameObject PosicionPuntos;//Seleciona en que objecto se guardara los objectos puntos
    public GameObject PuntoArriba;
    public GameObject PuntoAbajo;
    public GameObject AparecerCarritos;
    public bool bandera;
    private int cont;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            cont++;
            if ((cont == 1)&&(bandera==false))
            {
                GameObject cubo = Instantiate(CrearPunto, PuntoArriba.transform.position, Quaternion.identity);
                cubo.transform.parent = PosicionPuntos.transform;
                cubo.name = "Punto" + cont;
            }
            if ((cont == 2)&& (bandera == false))
            {
                GameObject cubo = Instantiate(CrearPunto, PuntoAbajo.transform.position, Quaternion.identity);
                cubo.transform.parent = PosicionPuntos.transform;
                cubo.name = "Punto" + cont;
            }
            if ((cont == 3) && (bandera))
            {
                GameObject cubo = Instantiate(CrearPunto, PuntoArriba.transform.position, Quaternion.identity);
                cubo.transform.parent = PosicionPuntos.transform;
                cubo.name = "Punto" + cont;
            }
            if ((cont == 4) && (bandera))
            {
                GameObject cubo = Instantiate(CrearPunto, PuntoAbajo.transform.position, Quaternion.identity);
                cubo.transform.parent = PosicionPuntos.transform;
                cubo.name = "Punto" + cont;
                AparecerCarritos.SetActive(true);
            }
        }
        
    }
}
