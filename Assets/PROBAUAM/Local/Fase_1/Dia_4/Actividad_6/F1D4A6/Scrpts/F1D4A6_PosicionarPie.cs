﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_PosicionarPie : MonoBehaviour
{
    public GameObject mano;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(mano.transform.position.x, mano.transform.position.y, mano.transform.position.z);
        this.transform.rotation = new Quaternion(mano.transform.rotation.x, mano.transform.rotation.y, mano.transform.rotation.z, mano.transform.rotation.w);
        this.transform.parent = mano.transform;
    }
}
