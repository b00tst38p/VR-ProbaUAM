﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class F1D4A6_GiaPies : MonoBehaviour
{
    public Rig Left;
    public Rig Right;
    public Transform RightTarjet;
    public Transform LeftTarjet;
    public Transform UpLeftTarjet;
    public Transform DownLeftTarjet;
    public Transform UpRightTarjet;
    public Transform DownRightTarjet;
    private float cont;
    private float tL;
    private float tR;
    public bool tiempo = true;
    public bool ArrAb = true;
    public bool cambio = true;
    public bool toco = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (cont > 1)
        {
            if (cambio)
            {
                tR = 1f;
            }
            else
            {
                tL = 1f;
            }
            cont = 0f;
            tiempo = false;
        }
        if (tiempo)
        {
            cont += Time.deltaTime;
        }
        if ((Right.weight <= 0.1) && (cambio) && (ArrAb) && (toco))
        {
            RightTarjet.position = UpRightTarjet.transform.position;
            //RightHit.position = DownRightHit.transform.position;
            tiempo = true;
            toco = false;
        }
        if ((Right.weight <= 0.1) && (cambio) && (ArrAb==false) && (toco))
        {
            RightTarjet.position = DownRightTarjet.transform.position;
            //RightHit.position = UpRightHit.transform.position;
            tiempo = true;
            toco = false;
        }
        if ((Left.weight <= 0.1) && (cambio==false) && (ArrAb) && (toco))
        {
            LeftTarjet.position = UpLeftTarjet.transform.position;
            //LefttHit.position = UpLeftHit.transform.position;
            tiempo = true;
            toco = false;
        }
        if ((Left.weight <= 0.1) && (cambio==false) && (ArrAb==false) && (toco))
        {
            LeftTarjet.position = DownLeftTarjet.transform.position;
            //LefttHit.position = DownLeftHit.transform.position;
            tiempo = true;
            toco = false;
        }

        Left.weight = Mathf.Lerp(Left.weight, tL, Time.deltaTime * 5f);
        Right.weight = Mathf.Lerp(Right.weight, tR, Time.deltaTime * 5f);
    }

    public void UpRbajar()
    {
        tR = 0f;
        ArrAb = false;
        toco = true;
    }
    public void DownRbajar()
    {
        Debug.Log("Entro");
        tR = 0f;
        ArrAb = true;
        cambio = false;
        toco = true;
    }
    public void UpLbajar()
    {
        tL = 0f;
        ArrAb = false;
        toco = true;
    }
    public void DownLbajar()
    {
        tL = 0f;
        ArrAb = true;
        cambio = true;
        toco = true;
    }
}
