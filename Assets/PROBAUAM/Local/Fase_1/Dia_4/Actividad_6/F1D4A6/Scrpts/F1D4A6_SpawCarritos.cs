﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_SpawCarritos : MonoBehaviour
{
    public List<GameObject> Objectos;
    public float TiempoAparecer;
    public GameObject PuntoSpawn;
    public float TiempoAparecerPunto;
    private float cont;

    private GameObject LugarCarros;
    // Start is called before the first frame update
    void Awake()
    {
        LugarCarros = GameObject.Find("Carritos");
    }

    // Update is called once per frame
    void Update()
    {
        LugarCarros = GameObject.Find("Carritos");
        GameObject carros;
        cont += Time.deltaTime;
        if (cont >= TiempoAparecer)
        {
            int random = Random.Range(0,Objectos.Count);
            cont = 0;
            carros = Instantiate(Objectos[random],transform.position,transform.rotation);
            carros.transform.parent = LugarCarros.transform;

        }
        if (cont >= TiempoAparecerPunto)
        {
            PuntoSpawn.gameObject.SetActive(true);
        }
    }
}
