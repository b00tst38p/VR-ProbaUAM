﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_CambioBagoneta : MonoBehaviour
{
    private F1D4A6_MoverCarroPalyer mover;
    private GameObject Jugador;
    public Transform PuntoDerecha;
    public Transform PuntoIzquierda;
    public GameObject CambiarPie;
    public GameObject Objectivo;
    public List<Material> Materiales;
    public F1D4A6_GiaPies GiaPies;
    //public float velocidad=1;
    // Start is called before the first frame update
    void Awake()
    {
        mover = GameObject.Find("Bagoneta").GetComponent<F1D4A6_MoverCarroPalyer>();
        Jugador = GameObject.Find("Jugador");
        Objectivo = GameObject.Find("Objectivo");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "F1D4A6_Rojo")
        {
            Objectivo.GetComponent<Renderer>().material = Materiales[2];
        }
        if (other.name == "F1D4A6_Amarillo")
        {
            Objectivo.GetComponent<Renderer>().material = Materiales[1];
        }
        if (other.name == "F1D4A6_Verde")
        {
            Objectivo.GetComponent<Renderer>().material = Materiales[0];
        }

        if (other.name == "Punto1")
        {
            GiaPies.UpRbajar();
            Jugador.transform.position = PuntoIzquierda.transform.position;
            CambiarPie.gameObject.SetActive(false);
        }
        if (other.name == "Punto3")
        {
            GiaPies.UpLbajar();
            Jugador.transform.position = PuntoDerecha.transform.position;
            CambiarPie.gameObject.SetActive(false);
        }
        if(other.name == "Punto2")
        {
            GiaPies.DownRbajar();
            CambiarPie.gameObject.SetActive(true);
        }
        if (other.name == "Punto4")
        {
            GiaPies.DownLbajar();
            CambiarPie.gameObject.SetActive(true);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
