﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_AvanzarCarrito : MonoBehaviour
{
    public float velocidad;
    public GameObject Carrito;//su mismo objeto pero desde el padre

    //private SistemaPuntaje puntaje;
    // Start is called before the first frame update
    void Awake()
    {
        //puntaje = GameObject.Find("Canvas").GetComponent<SistemaPuntaje>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Bagoneta")
        {
            Destroy(Carrito);
        }
        if(other.name == "Rock")
        {
            Destroy(Carrito);
            //puntaje.contador+=puntaje.sumar;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0f, 0f, -1f) * (velocidad * Time.deltaTime));
    }
}
