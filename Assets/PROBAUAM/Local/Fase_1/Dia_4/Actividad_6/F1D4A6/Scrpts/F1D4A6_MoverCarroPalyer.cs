﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;


public class F1D4A6_MoverCarroPalyer : MonoBehaviour
{
    // Start is called before the first frame update
    private float velocidad=0;
    public Transform CarritoJugador;//su mismo objeto pero desde el padre
    public GameObject creacion;//Objecto vacio donde se apareceran los carritos cuando se destruyan
    public Rig rigHead;
    public Transform TarjetHead;
    private Transform Player;
    private float t;
    private float tiempo = 0;
    private bool choco=false;

    //private SistemaPuntaje puntaje;
    private GameObject SpawnCarritos;
    private float cont;
    private bool bandera=false;
    private bool regresar=false;

    private GameObject Carritos;//Donde Apareceran los carritos
    void Awake()
    {
        SpawnCarritos = GameObject.Find("SpawnCarritos");
        Carritos = GameObject.Find("Carritos");

        //puntaje= GameObject.Find("Canvas").GetComponent<SistemaPuntaje>();
    }
    private void Start()
    {
        Player = GameObject.Find("Player").transform;
        SpawnCarritos.SetActive(false);
        TarjetHead.position = Player.transform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Rock")
        {
            t = 1f;
            choco = false;
            tiempo = 0;
            SpawnCarritos.SetActive(false);
            Destroy(Carritos);
            GameObject c = Instantiate(creacion);
            c.name = "Carritos";
            c.transform.parent = SpawnCarritos.transform;
            regresar = true;
            /*if (puntaje.contador <= 9)
            {
                puntaje.contador = 0;
            }
            else
            {
                puntaje.contador -= 10;
            }*/
        }
        if (other.name == "Bagon")
        {
            choco = true;
            t = 1f;
            //puntaje.chocar();
            velocidad = 1f;
            bandera = true;
        }
        if (other.name == "PuntoInicialJugador")
        {
            regresar = false;
            t = 0;
            SpawnCarritos.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Bagon")
        {
            velocidad = 0f;
        }
    }
    // Update is called once per frame
    void Update()
    {
        TarjetHead.position = Player.transform.position;
        if (choco)
        {

            tiempo += Time.deltaTime;
        }
        if (tiempo > 2)
        {
            t = 0f;
            choco = false;
            tiempo = 0;
        }
        rigHead.weight = Mathf.Lerp(rigHead.weight, t, Time.deltaTime * 10f);
        if (bandera)
        {

            Carritos = GameObject.Find("Carritos");
            cont += Time.deltaTime;
            
        }
        if (regresar)
        {
            CarritoJugador.transform.Translate(new Vector3(1f, 0f, 0f) * (1 * Time.deltaTime));
        }
        CarritoJugador.transform.Translate(new Vector3(-1f, 0f, 0f) * (velocidad * Time.deltaTime));
        if (cont >= 1)
        {
            bandera = false;
            cont = 0;
            velocidad = 0f;
        }
    }
}
