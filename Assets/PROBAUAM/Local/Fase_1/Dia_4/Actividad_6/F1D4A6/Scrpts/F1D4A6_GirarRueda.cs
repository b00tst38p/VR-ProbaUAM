﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A6_GirarRueda : MonoBehaviour
{
    private float velocidad = 30f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(velocidad, 0f, 0f) * (30 * Time.deltaTime));
    }
}
