﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class F1D4A4_SC_ControladorCliente : MonoBehaviour
{
    public NavMeshAgent agent;

    public GameObject objetivo1;
    public GameObject objetivo2;
    private Vector3 posObjetivo;
    private Animator animacion;

    public List<AudioClip> audios;
    public F1D4A4_SC_ControladorPedido controlPedido;

    private bool llegoDestino;
    private bool destino;
    // Start is called before the first frame update
    void Start()
    {
        animacion = gameObject.GetComponent<Animator>();
        llegoDestino = false;
        destino = false;
        animacion.SetBool("Caminar", true);
        objetivo1 = GameObject.Find("Puesto");
        gameObject.transform.rotation = objetivo1.transform.rotation;
        objetivo2 = GameObject.Find("Salida");
        controlPedido = GameObject.Find("ContenedorIngrediente").GetComponent<F1D4A4_SC_ControladorPedido>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!destino)
        {
            posObjetivo = objetivo1.transform.position;
            agent.destination = posObjetivo;
            destino = true;
        }


        if (gameObject.transform.position == objetivo1.transform.position && !llegoDestino)
        {
            llegoDestino = true;
            animacion.SetBool("Caminar", false);
            if (audios[0] == null)
            {
                controlPedido.generarPedido(client: gameObject);
                reproducirAudio(audios[0]);
            }

        }
    }

    public void reproducirAudio(AudioClip audio)
    {
        AudioSource.PlayClipAtPoint(audio, gameObject.transform.position, 1);
    }
    public void terminarProducto()
    {
        //llegoDestino = false;
        reproducirAudio(audios[1]);
        animacion.SetBool("Caminar", true);
        posObjetivo = objetivo2.transform.position;
        agent.destination = posObjetivo;
        agent.speed = 2;
        Destroy(gameObject, 2);
    }

   
}
