﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A4_SC_ControladorPedido : MonoBehaviour
{
    public F1D4A4_SC_ListaPedidos pedidos;
    private Pedido pedido;
    public Transform ubicacionPedido;
    public F1D4A4_SC_ControladorCliente cliente;
    [SerializeReference]
    public List<Transform> respawns;
    public List<AudioClip> audios;
    public GameObject nuevocliente;
    public Transform posCliente;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void generarPedido(GameObject client)
    {
        int indexPedido = Random.Range(0, pedidos.pedidos.Count);
        Pedido selectPedido = pedidos.pedidos[indexPedido];
        GameObject prefab = Instantiate(selectPedido.prefab);
        prefab.transform.position = ubicacionPedido.position;
        prefab.transform.rotation = ubicacionPedido.rotation;

        pedido = new Pedido(selectPedido.audioPedido, selectPedido.audioGracias, prefab, selectPedido.productosTotales, selectPedido.ingredietes);

        cliente = client.GetComponent<F1D4A4_SC_ControladorCliente>();
        crearIngredientes();

        cliente.audios[0] = pedido.audioPedido;
        cliente.audios[1] = pedido.audioGracias;

    }

    private void crearIngredientes()
    {
        int i = 0;
        GameObject ingrediente;
        foreach (GameObject g in pedido.ingredietes)
        {
            ingrediente = Instantiate(g);
            ingrediente.transform.position = respawns[i].position;
            i++;

        }
    }
    public void OnTriggerEnter(Collider other)
    {
        foreach (GameObject g in pedido.ingredietes)
        {
            if (other.name.Equals(g.name + "(Clone)"))
            {

                Destroy(other.gameObject);
                pedido.productosColocados++;
                if (pedido.productosColocados == pedido.productosTotales)
                {
                    pedido.prefab.SetActive(true);
                    gameObject.SetActive(false);
                    AudioSource.PlayClipAtPoint(audios[1], gameObject.transform.position, 1);
                }
                else
                {
                    AudioSource.PlayClipAtPoint(audios[0], gameObject.transform.position, 1);
                }
            }
        }
    }
    public void entregarPedido()
    {
        if (pedido.productosColocados >= pedido.productosTotales)
        {
            gameObject.SetActive(true);
            Destroy(pedido.prefab);
            pedido = null;
            cliente.terminarProducto();
            cliente = null;
            crearCliente();

        }
        else
        {
            AudioSource.PlayClipAtPoint(audios[2], gameObject.transform.position, 1);
        }

    }
    public void crearCliente()
    {
        Instantiate(nuevocliente, posCliente);

    }
  
}
