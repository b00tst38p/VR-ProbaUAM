﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class F1D4A4_SC_ListaPedidos : MonoBehaviour
{
    public List<Pedido> pedidos = new List<Pedido>();
}
[System.Serializable]
public class Pedido
{
    public AudioClip audioPedido;
    public AudioClip audioGracias;
    public GameObject prefab;
    public int productosTotales;
    public int productosColocados;
    public List<GameObject> ingredietes;
    public List<AudioClip> audiosHombre;
    public List<AudioClip> audioMujeres;
    public Pedido(AudioClip aud, AudioClip aud2, GameObject pfs, int producT, List<GameObject> ingr)
    {
        audioPedido = aud;
        audioGracias = aud2;
        prefab = pfs;
        productosTotales = producT;
        ingredietes = ingr;
        productosColocados = 0;
    }
}