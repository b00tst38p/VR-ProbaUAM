﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Global_SC_Manager : MonoBehaviour
{
    public static Global_SC_Manager manager = null;
    public static GameObject player = null;
    //private static GameObject player = null;
    void Awake()
    {
        
        //////////Cuando el script despierta verificamos si manager es diferente de nulo, es decir si ya existe, en cuyo caso destruimos este objeto

        if (manager != null)
        {         
            DestroyImmediate(gameObject);
        }
        /////////asignamos a manager la instancia actualmente activa y le indicamos que no debe serdestruida cuando se cargue otra escena
        else if (manager == null)
        {
            manager = this;           
        }        
        DontDestroyOnLoad(manager);
        
       reseteo();
    }


    public static void reseteo()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        Debug.Log(players.Length);
        if (player != null)
        {
            if (players.Length > 1)
            {
                Destroy(players[1].gameObject);
               
            }
        }
        else
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        


    }


}
