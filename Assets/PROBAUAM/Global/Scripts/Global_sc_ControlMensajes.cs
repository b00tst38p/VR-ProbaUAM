﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Global_sc_ControlMensajes : MonoBehaviour
{
    // Start is called before the first frame update
    public string[] mensajeSaludo;
    public string[] mensajeInstrucciones;
    public string[] mensajeIniciar;
    public string[] mensajeTerminar;
    public string[] mensajeDespedida;
    public TextMeshProUGUI txt;
    public GameObject panel;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator MostrarMensaje(string[] mensajes, float tiempo)
    {
        if (mensajes.Length>0)
        {
            EsVisible(true);
            float espera = tiempo / mensajes.Length;
            foreach (string msj in mensajes)
            {
                txt.text = msj;
                yield return new WaitForSeconds(espera);
            }
        }
        else
        {
            EsVisible(false);
            yield return new WaitForSeconds(tiempo);
            EsVisible(true);
        }
    }

    public void EsVisible(bool visible)
    {
        if (visible)
        {
            panel.SetActive(true);
        }
        else
        {
            panel.SetActive(false);
        }
    }

    public void Saludo(float tiempo)
    {
        panel.SetActive(true);
        StartCoroutine(MostrarMensaje(mensajeSaludo, tiempo));
    }

    public void Instrucciones(float tiempo)
    {
        StartCoroutine(MostrarMensaje(mensajeInstrucciones, tiempo));
    }

    public void Iniciar(float tiempo)
    {
        StartCoroutine(MostrarMensaje(mensajeIniciar, tiempo));
    }

    public void Terminar(float tiempo)
    {
        StartCoroutine(MostrarMensaje(mensajeTerminar, tiempo));
    }

    public void Despedida(float tiempo)
    {
        StartCoroutine(MostrarMensaje(mensajeDespedida, tiempo));
    }
}
