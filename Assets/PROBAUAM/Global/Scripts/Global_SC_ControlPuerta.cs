﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;



public class Global_SC_ControlPuerta : MonoBehaviour
{  
    //La variable tipo almacenara el tipo de puerta:
    //Puerta tipo 0: para las puertas de las escenas de Inicio y Calibración.
    //Puerta tipo 1: para las puertas de la escena de las Fases.
    //Puerta tipo 2: para las puertas de la escena de los Dias.
    //Puerta tipo 3: para las puertas de la escena de Actividades.
    //Puerta tipo 4: para la puerta de la escena de cada actividad que este realizando. 
    public int tipo;
    [SerializeField]
    public EstadosPuerta estado;
    //Para el control de las puertas activas y no activas


    //Para llevar un control de en que puertas esta entrando
    public int valor;

    private bool cambiar;
    private string escena;

    public Text texto;
    public TextMeshProUGUI txt;
    public string nombre;



    void Start()
    {
        
        texto.text = nombre;
        txt.text = nombre;
        cambiar = true;
        if (estado == EstadosPuerta.Construccion)
        {
            puertaEnConstruccion(); 
        }
        if (estado == EstadosPuerta.Bloqueada)
        {
            bloquearPuerta();
        }

    }


    public void AbrirPuerta()
    {
        if (cambiar)
        {
            if (estado != EstadosPuerta.Bloqueada)
            {
                
                    cambioEscena();
                    cambiar = false;
                

            }
        }
    }
    public void bloquearPuerta()
    {
        estado = EstadosPuerta.Bloqueada;
        transform.GetChild(1).gameObject.SetActive(true);
        transform.GetChild(2).gameObject.SetActive(false);
    }

    public void puertaEnConstruccion()
    {
        estado = EstadosPuerta.Construccion;
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(true);
    }

    public void activarPuerta()
    {
        if (estado != EstadosPuerta.Construccion)
        {
            estado = EstadosPuerta.Activa;
        }
       
    }

    public void setEstado(int activasP)
    {

        if (valor > activasP && activasP != 0)
        {
            bloquearPuerta();
        }
        else
        {
            if (estado != EstadosPuerta.Construccion)
            {
                estado = EstadosPuerta.Activa;
            }            
        }


    }

    private void Salir()
    {
        switch (valor)
        {
            case 0:
                escena = "Inicio";                
                break;
            case 1:
                escena = "Inicio";
                Global_SC_Settings.ubicacion_stc.dia = 1;
                break;
            case 2:
                escena = "Dias";
                Global_SC_Settings.ubicacion_stc.actividad = 1;
                break;
            case 3:
                escena = "Actividades";          
                break;            
            default:
                break;
        }
        SceneManager.LoadSceneAsync(escena);
        //ReseteoPlayer.reseteo();
    }

    public void cambioEscena()
    {
        switch (tipo)
        {
            case -1:
                Salir();
                break;
            case 0:
                escena = nombre;
                break;
            case 1:
                Global_SC_Settings.ubicacion_stc.fase = valor;
                ComprobarFase();                
                break;
            case 2:
                Global_SC_Settings.ubicacion_stc.dia = valor;
                ComprobarActividad();

                break;
            case 3:
                Global_SC_Settings.ubicacion_stc.actividad = valor;
                ComprobarEstatusActividad();

                break;
            case 4:
                CambioActividad();
                break;
            default:
                break;
        }
        SceneManager.LoadSceneAsync(escena);
        //ReseteoPlayer.reseteo();
    }

    public void irA_Scena()
    {
        escena = nombre;
        SceneManager.LoadSceneAsync(escena);
        //ReseteoPlayer.reseteo();
    }

    private void ComprobarFase()
    {
        if (Global_SC_Settings.usuario_stc.fase == 0)
        {
            Global_SC_Settings.usuario_stc.fase = 1;
            escena = "Calibracion";
        }
        else if (Global_SC_Settings.usuario_stc.dia == 0)
        {
            Global_SC_Settings.ubicacion_stc.dia = 1;
            Global_SC_Settings.usuario_stc.dia = 1;
            escena = "Dias";
        }
        else if (Global_SC_Settings.usuario_stc.actividad == 0)
        {
            escena = "Dias";
        }
        else
        {
            escena = "Actividades";
        }
    }   

    private void ComprobarActividad()
    {
        if (Global_SC_Settings.usuario_stc.actividad == 0)
        {
            Global_SC_Settings.ubicacion_stc.actividad = 1;
            Global_SC_Settings.usuario_stc.actividad = 1;
           
        }
        escena = "Actividades";
    }

    private void ComprobarEstatusActividad()
    {
        if (Global_SC_Settings.isUltimaActividad())
        {
            Global_SC_Settings.ubicacion_stc.actividad = 0;
            Global_SC_Settings.ubicacion_stc.dia++;
            if (Global_SC_Settings.ubicacion_stc.dia > 6)
            {
                Global_SC_Settings.ubicacion_stc.dia = 0;
                Global_SC_Settings.ubicacion_stc.fase++;

                if (Global_SC_Settings.ubicacion_stc.fase > 3)
                {
                    Global_SC_Settings.ubicacion_stc.fase = 0;
                    escena = "Inicio";//mndariamos a algo representativo del fin de la terapia
                }
                else
                {
                    escena = "Inicio";
                }
            }
            else
            {
                escena = "Dias";
            }
        }
        else
        {
            
            escena = Global_SC_Settings.getNombreScena();
        }
        Debug.Log(escena + "   la escena es");
        ValidarProgreso();

    }

    private void CambioActividad()
    {
        Global_SC_Settings.ubicacion_stc.actividad++;
        ComprobarEstatusActividad();
    }

    private void ValidarProgreso()
    {
        if (Global_SC_Settings.usuario_stc.fase <= Global_SC_Settings.ubicacion_stc.fase)
        {
            Global_SC_Settings.usuario_stc.fase = Global_SC_Settings.ubicacion_stc.fase;
            Global_SC_Settings.usuario_stc.dia = Global_SC_Settings.ubicacion_stc.dia;
            Global_SC_Settings.usuario_stc.actividad = Global_SC_Settings.ubicacion_stc.actividad;

            if (Global_SC_Settings.usuario_stc.dia <= Global_SC_Settings.ubicacion_stc.dia)
            {
                Global_SC_Settings.usuario_stc.dia = Global_SC_Settings.ubicacion_stc.dia;
                Global_SC_Settings.usuario_stc.actividad = Global_SC_Settings.ubicacion_stc.actividad;

                if (Global_SC_Settings.usuario_stc.actividad <= Global_SC_Settings.ubicacion_stc.actividad)
                {
                    Global_SC_Settings.usuario_stc.actividad = Global_SC_Settings.ubicacion_stc.actividad;
                }
            }
        }


    }

}
