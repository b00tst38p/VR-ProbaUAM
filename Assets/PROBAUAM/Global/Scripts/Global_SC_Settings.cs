﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Usuarios
{
    public int fase;
    public int dia;
    public int actividad;

}

[System.Serializable]
public class Actividad
{
    public string nombreScena;
    public GameObject puerta;
    public bool habilitada;
}

[System.Serializable]
public class Dia
{
    public String nombre;
    public Actividad[] actividades;   
}

[System.Serializable]
public class Fase
{
    public String nombre;
    public GameObject puerta;
    public GameObject escenario;
    public GameObject escenario2;
    
    [SerializeField]
    public Dia[] dias = new Dia[6];
    
    
}

[System.Serializable]
public class Ubicacion
{
    public int fase;
    public int dia;
    public int actividad;
}

public enum EstadosPuerta
{
    Bloqueada,
    Activa,
    Seleccionada,
    Completada,
    Construccion
};


public enum EscenaActiva
{
    Inicio,
    Fase,
    Dia,
    Actividad,
    Juego,
};

[Serializable]
public class Global_SC_Settings : MonoBehaviour
{

    public static bool modoDios;

    public static Fase[] protocolo_stc;

    public static Ubicacion ubicacion_stc;

    public static Usuarios usuario_stc;

    public  static GameObject escenarioPincipal_stc;

    [SerializeField]
    public Fase[] protocolo;

    [SerializeField]
    public Ubicacion ubicacion;

    [SerializeField]
    public Usuarios usuario;

    public GameObject escenarioPincipal;

    public bool modoDiosP;


    void Awake()
    {
        if (protocolo_stc == null)
        {
            protocolo_stc = protocolo;
            usuario_stc = usuario;
            int fase = usuario.fase;
            int dia = usuario.dia;
            int act = usuario.actividad;
            ubicacion.fase = fase;
            ubicacion.dia = dia;
            ubicacion.actividad = act;
            ubicacion_stc = ubicacion;
            escenarioPincipal_stc = escenarioPincipal;
            
            modoDios = modoDiosP;
        }
        
    }


    public static int getNumeroPuertas(EscenaActiva escena)
    {
        if (escena == EscenaActiva.Actividad)
        {
            return protocolo_stc[ubicacion_stc.fase - 1].dias[ubicacion_stc.dia - 1].actividades.Length;
        }
        else if (escena == EscenaActiva.Dia)
        {
            return 6;
        }
        else if (escena == EscenaActiva.Fase)
        {
            return 3;
        }
        return 0;
    }

    public static GameObject getGOpuerta(EscenaActiva escena, int index)
    {
        GameObject puerta = null;
        if (escena == EscenaActiva.Actividad)
        {
            
                puerta = protocolo_stc[ubicacion_stc.fase - 1].dias[ubicacion_stc.dia - 1].actividades[index].puerta;
            
                        
        }
        else if (escena == EscenaActiva.Dia)
        {
            puerta = protocolo_stc[ubicacion_stc.fase - 1].puerta;
        }
        else if (escena == EscenaActiva.Fase)
        {
            puerta = protocolo_stc[index].puerta;
        }


        return puerta;
    }
    public static bool actividadHabilitada(int index)
    {
        Actividad act = protocolo_stc[ubicacion_stc.fase - 1].dias[ubicacion_stc.dia - 1].actividades[index];
        return act.habilitada;
    }

    public static GameObject getEscenario(int escenario)
    {
        if (escenario == -1)
        {
            return protocolo_stc[ubicacion_stc.fase - 1].escenario;
        }

        return protocolo_stc[escenario].escenario;
    }

    public static GameObject getEscenario2(int escenario)
    {
        
        if (escenario == -1)
        {
            return protocolo_stc[ubicacion_stc.fase - 1].escenario2;
        }
        else if (escenario < 3)
        {
            return protocolo_stc[escenario].escenario2;
        }
        else
        {
            return escenarioPincipal_stc;
        }

        
        
    }

    public static int getPuertaDelante(EscenaActiva escena)
    {
        if (escena == EscenaActiva.Fase)
        {
            return ubicacion_stc.fase - 1;
        }
        else if (escena == EscenaActiva.Dia)
        {
            return ubicacion_stc.dia - 1;
        }
        else if (escena == EscenaActiva.Actividad)
        {
            return ubicacion_stc.actividad - 1;
        }
        return 0;
    }

    public static string getNombreScena()
    {
        Actividad actividad = protocolo_stc[ubicacion_stc.fase - 1].dias[ubicacion_stc.dia - 1].actividades[ubicacion_stc.actividad - 1];
        if (actividad.habilitada)
        {
            return actividad.nombreScena;
        }
        return "Default";
    }

    public static bool isUltimaActividad()
    {
        return ubicacion_stc.actividad > protocolo_stc[ubicacion_stc.fase - 1].dias[ubicacion_stc.dia - 1].actividades.Length;
    }

   




}
