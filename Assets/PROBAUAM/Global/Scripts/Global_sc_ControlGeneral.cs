﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public enum ModoTerminacion
{
    Tiempo,
    Puntaje,
    Repeticiones
}

public enum Medalla
{
    Oro,
    Plata,
    Bronce
}

public enum Instrucciones
{
    Automatico,
    Personalizado,
    Ninguna
}

public class Global_sc_ControlGeneral : MonoBehaviour
{
    [Label("Ver al Jugador")]
    public bool LookAtPlayer;

    [Header("---MAIN FLOW SETTINGS---")]
    [Label("Previo a Bienvenida")]
    public float espera1;
    public bool bienvenida;
    [Label("Transicion Bienvenida-Instrucciones")]
    public float espera2;
    public Instrucciones instrucciones;
    public float tiempoInstruccion;
    [Label("Transicion Instrucciones-Juego")]
    public float espera3;
    public ModoTerminacion terminacion;
    [ShowIf("metaElegida")]
    public Medalla puntajeMeta;
    [Label("Transicion Juego-Terminado")]
    public float espera4;
    [Label("Transicion Terminado-Despedida")]
    public float espera5;

    [Header("---MAIN FLOW STAGES---")]
    public bool bienvenidaCompletada;
    public bool instruccionCompletada;
    public bool cuentaCompletada;
    public bool inicioCompletado;
    public bool juegoCompletado;
    public bool terminacionCompletado;
    public bool despedidaCompletado;
    

    private float puntuacion;
    private float aciertos;


    private bool metaElegida() {
        if (terminacion == ModoTerminacion.Puntaje)
            return true;
        else
            return false;
    }

    Global_sc_ControlPuntaje controlPuntaje;
    Global_sc_ControlTemporizador controlTemporizador;
    Global_sc_AudioController controlAudio;
    Global_sc_ControlComportamientos controlComportamientos;
    Global_sc_ControlMensajes controlMensajes;
    // Start is called before the first frame update
    void Start()
    {
        controlPuntaje = GetComponent<Global_sc_ControlPuntaje>();
        controlTemporizador = GetComponent<Global_sc_ControlTemporizador>();
        controlAudio = GetComponent<Global_sc_AudioController>();
        controlComportamientos = GetComponent<Global_sc_ControlComportamientos>();
        controlMensajes = GetComponent<Global_sc_ControlMensajes>();

        StartCoroutine(MainFlow());
    }
    public IEnumerator MainFlow()
    {
        float tAux;
        yield return new WaitForSeconds(espera1);

        //----Bienvenida----
        if (bienvenida)
        {
            controlComportamientos.Bienvenida();
            tAux = controlAudio.Saludo();
            controlMensajes.Saludo(tAux);
            yield return new WaitForSeconds(tAux);
            bienvenidaCompletada = true;
        }

        yield return new WaitForSeconds(espera2);

        //----Instrucciones----
        if (instrucciones == Instrucciones.Automatico)
        {
            controlAudio.Instrucciones();
            controlComportamientos.Instrucciones();
            controlMensajes.Instrucciones(tiempoInstruccion);
            yield return new WaitForSeconds(tiempoInstruccion);
            instruccionCompletada = true;
        }
        else if (instrucciones == Instrucciones.Personalizado) 
        {
            controlMensajes.EsVisible(false);
            yield return new WaitUntil(() => instruccionCompletada);
            //controlMensajes.EsVisible(true);
        }
        if(instrucciones == Instrucciones.Ninguna)
        {
            instruccionCompletada = true;
        }


        yield return new WaitForSeconds(espera3);

        //CUENTA REGRESIVA
        controlTemporizador.Iniciar();
        tAux = controlTemporizador.cuentaRegresiva;
        controlMensajes.EsVisible(false);
        yield return new WaitForSeconds(tAux);
        //controlMensajes.EsVisible(true);
        cuentaCompletada = true;


        //INICIO COMPORTAMIENTOS
        controlComportamientos.Iniciar();
        tAux = controlAudio.Iniciar();
        controlMensajes.Iniciar(tAux);
        yield return new WaitForSeconds(tAux);
        inicioCompletado = true;

        //Tiempo de juego
        switch (terminacion)
        {
            case ModoTerminacion.Tiempo:
                yield return new WaitForSeconds(controlTemporizador.duracion+1);
                break;
            case ModoTerminacion.Puntaje:
                if(puntajeMeta==Medalla.Oro)
                    yield return new WaitUntil(() => puntuacion >= controlPuntaje.puntajeOro);
                else if(puntajeMeta == Medalla.Plata)
                    yield return new WaitUntil(() => puntuacion >= controlPuntaje.puntajeOro);
                else
                    yield return new WaitUntil(() => puntuacion >= controlPuntaje.puntajeBronce);
                break;
            case ModoTerminacion.Repeticiones:
                yield return new WaitUntil(() => aciertos >= controlPuntaje.repeticionesEsperadas);
                break;
        }
        juegoCompletado = true;

        //Terminar juego
        controlComportamientos.Terminar();
        tAux = controlAudio.Terminar();
        controlMensajes.Terminar(tAux);
        controlAudio.repetirInstrucciones = false;
        yield return new WaitForSeconds(tAux);
        terminacionCompletado = true;


        yield return new WaitForSeconds(espera4);

        /////DESPEDIDA
        controlComportamientos.Despedida();
        tAux = controlAudio.Despedida();
        controlMensajes.Despedida(tAux);
        yield return new WaitForSeconds(tAux);
        despedidaCompletado = true;
    }

    public void AcertarPositivo()
    {
        controlPuntaje.Acertar(1);
    }

    public void AcertarNegativo()
    {
        controlPuntaje.Acertar(-1);
    }

    public void Puntar(float puntuacion)
    {
        controlPuntaje.Puntuar(puntuacion);
    }

    public void PuntarYMostrar(float puntuacion)
    {
        controlPuntaje.PuntuarYMostrar(puntuacion);
    }

    public void PuntarYMostrar(float puntuacion, Vector3 posicion)
    {
        controlPuntaje.PuntuarYMostrar(puntuacion, posicion);
    }

    // Update is called once per frame
    void Update()
    {
        if (LookAtPlayer)
        {
            transform.LookAt(Camera.main.transform);
        }
        puntuacion = controlPuntaje.puntuacionTotal;
        aciertos = controlPuntaje.aciertos;
    }




















    //public void Bienvenida()
    //{
    //    if (darBienvenida)
    //    {
    //        StartCoroutine(EjecutarBienvenida());
    //    }
    //}
    //public void Instrucciones()
    //{
    //    if (darInstrucciones)
    //    {
    //        StartCoroutine(EjecutarInstrucciones());
    //    }
    //}

    //public void Jugar()
    //{
    //    controlComportamientos.Iniciar();
    //}

    ////private IEnumerator EjecutarJuego()
    ////{

    ////}

    //private IEnumerator EjecutarInstrucciones()
    //{
    //    if (!instruccionesCompletada)
    //    {
    //        instruccionesCompletada = true;
    //        controlAudio.repetirInstrucciones = repetirInstrucciones;
    //        controlAudio.intervaloRepeticion = intervaloRepeticion;
    //        controlAudio.Instrucciones();
    //        controlComportamientos.Instrucciones();
    //        yield return new WaitForSeconds(tiempoInstruccion);
    //    }
    //    if (inicioAutomatico)
    //    {
    //        Jugar();
    //    }
    //}

    //private IEnumerator EjecutarBienvenida()
    //{
    //    if (!bienvenidaCompletada)
    //    {
    //        bienvenidaCompletada = true;
    //        yield return new WaitForSeconds(esperaPreviaBienvenida);
    //        controlComportamientos.Bienvenida();
    //        yield return new WaitForSeconds(controlAudio.Saludo());
    //    }

    //    if (darInstrucciones && instruccionesAutomaticas)
    //    {
    //        Instrucciones();
    //    }
    //}
}