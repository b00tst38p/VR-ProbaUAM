﻿using UnityEngine.Animations.Rigging;
using UnityEngine;

public class Global_Head : MonoBehaviour
{
    public RigBuilder RigBuilder;
    public bool CaraFija;
    public float tiempoEspera = 1;
    public float tiempo = 0;
    private string emotions;
    private int cont;

    private Animator m_Animator;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    
    void Update()
    {
        if (cont == 0)
        {
            RigBuilder.enabled = false;
            cont++;
        }
        if (cont == 1 && tiempo>tiempoEspera)
        {
            RigBuilder.enabled = true;
            cont++;
        }
            m_Animator.SetBool("Angry", false);
            m_Animator.SetBool("Fun", false);
            m_Animator.SetBool("Joy", false);
            m_Animator.SetBool("Sorrow", false);
            m_Animator.SetBool("Surprised", false);
        if (CaraFija)
        {
            m_Animator.SetBool(emotions, true);
        }
        else
        {
            emotions = Re(emotions);
        }


    }
    public string Re(string emo)
    {
        m_Animator.SetBool(emo, true);
        if (tiempo > tiempoEspera)
        {
            m_Animator.SetBool(emo, false);
            emo = "";
        }
        else
        {
            tiempo += Time.deltaTime;
        }
        return emo;
    }
    public void Angry(float tiempoEsp)
    {
        tiempo = 0;
        emotions = "Angry";
        tiempoEspera = tiempoEsp;
    }
    public void Fun(float tiempoEsp)
    {
        tiempo = 0;
        emotions = "Fun";
        tiempoEspera = tiempoEsp;
    }
    public void Joy(float tiempoEsp)
    {
        tiempo = 0;
        emotions = "Joy";
        tiempoEspera = tiempoEsp;
    }
    public void Sorrow(float tiempoEsp)
    {
        tiempo = 0;
        emotions = "Sorrow";
        tiempoEspera = tiempoEsp;
    }
    public void Surprised(float tiempoEsp)
    {
        tiempo = 0;
        emotions = "Surprised";
        tiempoEspera = tiempoEsp;
    }

}
