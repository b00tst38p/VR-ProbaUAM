﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_SC_EfectoDesvanecer : MonoBehaviour
{
    
    private bool desvanecer, bandera = true;
    

    private GameObject objeto_activo;
    private GameObject objeto_nuevo;
    private int tipo;

    public GameObject prefab;
    public Transform cooredenadas;
    public float velocidad;
    public bool terminoActividad;
    

    private void Start()
    {
        velocidad = 0.01f;
        bandera = true;
        objeto_nuevo = prefab;
        
    }
    
    
    // Update is called once per frame
    void Update()
    {


        if (terminoActividad)
        {
            terminarActividad();
            terminoActividad = false;
        }
       
            if (desvanecer)
            {
            if (tipo == 1)
            {
                cambioEscenario();
            }
            else if(tipo ==2)
            {
                setSiguiente();
            }
           
            }
        

        
            
        

    }

   
    private void cambioEscenario()
    {
        objeto_activo.transform.GetChild(0).gameObject.SetActive(false);
        if (bandera)
        {
            objeto_nuevo = Instantiate(objeto_nuevo);
            objeto_nuevo.transform.localScale = new Vector3(1, 0, 1);
            bandera = false;
        }

        objeto_activo.transform.localScale -= new Vector3(0, velocidad, 0);
        objeto_nuevo.transform.localScale += new Vector3(0, velocidad, 0);
        if (objeto_nuevo.transform.localScale.y >= 1)
        {
            desvanecer = false;
            Destroy(objeto_activo);
            objeto_activo = objeto_nuevo;
            objeto_nuevo = null;
            bandera = true;
        }
    }

    private void setSiguiente()
    {
        
        if (bandera)
        {
            objeto_nuevo = Instantiate(objeto_nuevo, cooredenadas);
            objeto_nuevo.transform.localScale = new Vector3(1, 0, 1);
            bandera = false;
        }        
        objeto_nuevo.transform.localScale += new Vector3(0, velocidad, 0);
        if (objeto_nuevo.transform.localScale.y >= 1)
        {
            desvanecer = false;            
            bandera = true;
        }

    }

    public void terminarActividad()
    {
        tipo = 2;
        desvanecer = true;
        
    }
    
    public void cambiarEscena(int esenario)
    {
        tipo = 1;
        if (objeto_activo==null)
        {
            objeto_activo = Instantiate(Global_SC_Settings.getEscenario2(esenario));
        }
        else if (!desvanecer)
        {
            objeto_nuevo = Global_SC_Settings.getEscenario2(esenario);       
            desvanecer = true;
        }
        
    }



}
