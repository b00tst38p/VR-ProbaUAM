﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum tiempo
{
    Cronometro,
    Temporizador
}

public class Global_sc_ControlTemporizador : MonoBehaviour
{
    public TextMeshProUGUI txtTemp;
    public TextMeshProUGUI txtTempText;
    //public float tiempoEspera;
    //public bool inicioAutomatico;
    //[HideInInspector]
    public float cuentaRegresiva;
    public tiempo TipoReloj;
    [HideInInspector]
    public float cronometro;
    public float duracion;
    [HideInInspector]
    public float contadorRegresiva;
    private bool jugando = false;

    private AudioClip countDownBip;
    private AudioClip countDownStart;
    public new AudioSource audio;

    void Start()
    {
        countDownBip = Resources.Load<AudioClip>("Sonido/UI2D/soft-bip");
        countDownStart = Resources.Load<AudioClip>("Sonido/UI2D/start-bip");
    }

    void Update()
    {
        
    }

    public bool estaJugando()
    {
        return jugando;
    }

    public void Iniciar()
    {
        StartCoroutine(IniciarTiempo());
    }

    public void Detener()
    {
        jugando = false;
        StopAllCoroutines();
    }

    private IEnumerator IniciarTiempo()
    {
        //yield return new WaitForSeconds(tiempoEspera);
        //if(inicioAutomatico)
        //{
        //    GetComponent<Global_sc_AudioController>().SaludoConInstrucciones();
        //}
        contadorRegresiva = cuentaRegresiva;
        txtTempText.text = "Comienza en:";
        while (contadorRegresiva >= 0)
        {
            txtTemp.text = contadorRegresiva.ToString();

            if (audio)
            {
                if (contadorRegresiva > 0)
                {
                    audio.PlayOneShot(countDownBip, .5f);
                }
                if(contadorRegresiva == 0)
                {
                    audio.PlayOneShot(countDownStart, .5f);
                }
            }
                
            
            yield return new WaitForSeconds(1);
            contadorRegresiva--;
        }
        jugando = true;

        StartCoroutine(Cronometro());
        StartCoroutine(Temporizador());
    }

    private IEnumerator Cronometro()
    {
        while (true)
        {
            if (TipoReloj == tiempo.Cronometro)
            {
                txtTemp.text = cronometro.ToString();
                txtTempText.text = "Transcurrido:";
            }
            yield return new WaitForSeconds(1);
            cronometro++;
        }
        
    }

    private IEnumerator Temporizador()
    {
        while (duracion>=0)
        {
            if (TipoReloj == tiempo.Temporizador)
            {
                txtTemp.text = duracion.ToString();
                txtTempText.text = "Restante:";
            }
            if (audio && duracion == 0)
            {
                //Se está utilizando el mismo audio que al inicio
                //Se puede cambiar
                audio.PlayOneShot(countDownStart, .5f);
            }
            yield return new WaitForSeconds(1);
            duracion--;
        }
    }
}
