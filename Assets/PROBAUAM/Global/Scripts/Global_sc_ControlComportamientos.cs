﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_sc_ControlComportamientos : MonoBehaviour
{
    [Header("---ABRIR JUEGO/BIENvENIDA---")]
    public List<Behaviour> activarBienvenida;
    public List<Behaviour> desactivarBienvenida;
    [Header("---INSTRUCCIONES JUEGO---")]
    public List<Behaviour> activarInstrucciones;
    public List<Behaviour> desactivarInstrucciones;
    [Header("---INICIAR JUEGO---")]
    public List<Behaviour> activarInicio;
    public List<Behaviour> desactivarInicio;
    [Header("---FINAL JUEGO---")]
    public List<Behaviour> activarFinal;
    public List<Behaviour> desactivarFinal;
    [Header("---DESPEDIDA---")]
    public List<Behaviour> activarDespedida;
    public List<Behaviour> desactivarDespedida;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Bienvenida()
    {
        ActivarAbrir();
        DesactivarAbrir();
    }
    public void Instrucciones()
    {
        DesactivarInstrucciones();
        ActivarInstrucciones();
    }

    public void Iniciar()
    {
        DesactivarInicio();
        ActivarInicio();
    }

    public void Terminar()
    {
        DesactivarFin();
        ActivarFin();
    }
    public void Despedida()
    {
        DesactivarDespedida();
        ActivarDespedida();
    }

    private void ActivarAbrir()
    {
        if (activarBienvenida == null)
            return;
        foreach (Behaviour comp in activarBienvenida)
        {
            comp.enabled = true;
        }
    }

    private void DesactivarAbrir()
    {
        if (desactivarBienvenida == null)
            return;
        foreach (Behaviour comp in desactivarBienvenida)
        {
            comp.enabled = false;
        }
    }
    private void ActivarInstrucciones()
    {
        if (activarInstrucciones == null)
            return;
        foreach (Behaviour comp in activarInstrucciones)
        {
            comp.enabled = true;
        }
    }

    private void DesactivarInstrucciones()
    {
        if (desactivarInstrucciones == null)
            return;
        foreach (Behaviour comp in desactivarInstrucciones)
        {
            comp.enabled = false;
        }
    }

    private void ActivarInicio()
    {
        if (activarInicio == null)
            return;
        foreach (Behaviour comp in activarInicio)
        {
            comp.enabled = true;
        }
    }

    private void DesactivarInicio()
    {
        if (desactivarInicio == null)
            return;
        foreach (Behaviour comp in desactivarInicio)
        {
            comp.enabled = false;
        }
    }

    private void ActivarFin()
    {
        if (activarFinal == null)
            return;
        foreach (Behaviour comp in activarFinal)
        {
            comp.enabled = false;
        }
    }

    private void DesactivarFin()
    {
        if (desactivarFinal == null)
            return;
        foreach (Behaviour comp in desactivarFinal)
        {
            comp.enabled = false;
        }
    }

    private void ActivarDespedida()
    {
        if (activarDespedida == null)
            return;
        foreach (Behaviour comp in activarDespedida)
        {
            comp.enabled = false;
        }
    }

    private void DesactivarDespedida()
    {
        if (desactivarDespedida == null)
            return;
        foreach (Behaviour comp in desactivarDespedida)
        {
            comp.enabled = false;
        }
    }
}
