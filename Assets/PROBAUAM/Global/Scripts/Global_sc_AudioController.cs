﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_sc_AudioController : MonoBehaviour
{
    public AudioSource audioSource;
    [Header("---BIENVENIDA---")]
    public AudioClip audioBienvenida;
    //public bool inicioAutomático;
    //public float esperaPrevia;
    //public float esperaPosterior;
    [Header("---INSTRUCCIONES---")]
    public AudioClip[] audioInstrucciones;
    //public bool continuarAutomatico;
    public bool repetirInstrucciones;
    public float intervaloRepeticion;
    [Header("---Inicio---")]
    public AudioClip audioInicio;
    [Header("---Fin---")]
    public AudioClip audioFin;
    [Header("---DESPEDIDA---")]
    public AudioClip audioDespedida;
    

    //[HideInInspector]
    //public bool saludo;
    //[HideInInspector]
    //public bool instrucciones;
    //[HideInInspector]
    //public bool despedida;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float Saludo()
    {
        //StartCoroutine(reproducirBienvenida());
        if (audioBienvenida != null)
        {
            //yield return new WaitForSeconds(esperaPrevia);
            audioSource.PlayOneShot(audioBienvenida);
            //yield return new WaitForSeconds(audioBienvenida.length);
            return audioBienvenida.length;
        }
        return 0;
    }

    public void Instrucciones()
    {
        StartCoroutine(reproducirInstrucciones());
    }

    private IEnumerator reproducirInstrucciones()
    {
        do
        {
            if (audioInstrucciones != null)
            {
                foreach (AudioClip audio in audioInstrucciones)
                {
                    audioSource.PlayOneShot(audio);
                    yield return new WaitForSeconds(audio.length + 2);
                }
                yield return new WaitForSeconds(intervaloRepeticion);
            }
            else
            {
                break;
            }
            
        } while (repetirInstrucciones);
    }

    public float Iniciar()
    {
        if (audioInicio != null)
        {
            audioSource.PlayOneShot(audioInicio);
            return audioInicio.length;
        }
        return 0;
    }

    public float Terminar()
    {
        if (audioFin)
        {
            audioSource.PlayOneShot(audioFin);
            return audioFin.length;
        }
        return 0;
    }


    //private IEnumerator reproducirBienvenidaConInstrucciones()
    //{
    //    //StartCoroutine(reproducirBienvenida());
    //    Saludo();

    //    if (audioBienvenida != null)
    //    {
    //        yield return new WaitForSeconds(audioBienvenida.length);
    //    }
        
    //    StartCoroutine(reproducirInstrucciones());
    //}

    public float Despedida()
    {
        if (audioDespedida != null)
        {
            audioSource.PlayOneShot(audioDespedida);
            return audioDespedida.length;            
        }
        return 0;
    }
}
