﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Global_sc_ControlPuntaje : MonoBehaviour
{
    [Header("---NO MODIFICAR---")]

    public TextMeshProUGUI puntuacionTxt;
    public Transform barraCarga;
    public GameObject estrellaOro;
    public GameObject estrellaPlata;
    public GameObject estrellaBronce;
    public Global_sc_AudioController controlAudio;

    bool ganasteBronce = false;
    bool ganastePlata = false;
    bool ganasteOro = false;

    //variable temporal para colocar los puntos obtenidos en algun lugar visible
    public GameObject posicionDefault;
    //esta puntuacion total se almacena para cada actividad
    [HideInInspector]
    public float puntuacionTotal = 0;
    [HideInInspector]
    public float aciertos = 0;



    [Header("---MODIFICAR---")]
    public float puntajeOro;
    public float puntajePlata;
    public float puntajeBronce;
    public float repeticionesEsperadas;
    /*DEBUGEO
    public float puntajeAumento;
    public float puntajeDeResta;
    */


    void Start()
    {
        puntuacionTxt.text = puntuacionTotal.ToString();
        barraCarga.GetComponent<Image>().fillAmount = puntuacionTotal / puntajeOro;
        //posicion = GameObject.Find("GameObject");
    }


    void Update()
    {
        //PARA DEBUGEO MANUAL
        if (Input.GetKeyDown("up"))
        {
            //Puntuar(10);
            PuntuarYMostrar(10, posicionDefault.transform.position);
            //Sistema_UIPuntos.GenerarUI(posicion.transform.position, "+10");
            //aumentarPuntaje(puntajeAumento, puntajeOro);
        }

        if (Input.GetKeyDown("down"))
        {
            //Puntuar(-10);
            PuntuarYMostrar(-10, posicionDefault.transform.position);
            //Sistema_UIPuntos.GenerarUI(posicion.transform.position, "-10");
            //restarPuntaje(puntajeDeResta);
        }


        puntuacionTxt.text = puntuacionTotal.ToString();

        /*if (ganasteOro)
        {
            if (controlAudio != null)
            {
                if (!controlAudio.despedida)
                    StartCoroutine(controlAudio.reproducirDespedida());
            }

            //Sistema_CambioEscenas.CambioEscena("LobbyDEMO");
        }*/
    }

    public void Acertar(float valorDelAcierto)
    {
        aciertos += valorDelAcierto;
    }

    public void PuntuarYMostrar(float puntosDeAumento)
    {
        PuntuarYMostrar(puntosDeAumento, posicionDefault.transform.position);
    }

    public void PuntuarYMostrar(float puntosDeAumento, Vector3 posicion)
    {
        if (posicion != null)
        {
            if (puntosDeAumento < 0)
                Sistema_UIPuntos.GenerarUI(posicion, puntosDeAumento.ToString());
            else
                Sistema_UIPuntos.GenerarUI(posicion, "+" + puntosDeAumento.ToString());
        }
        Puntuar(puntosDeAumento);
    }

    public void Puntuar(float puntosDeAumento)
    {
        puntuacionTotal += puntosDeAumento;

        if (puntuacionTotal < 0)
            puntuacionTotal = 0;

        barraCarga.GetComponent<Image>().fillAmount = puntuacionTotal / puntajeOro;
        if (puntuacionTotal >= puntajeBronce && puntuacionTotal < puntajePlata && ganasteBronce == false)
        {
            estrellaBronce.GetComponent<Animation>().Play();
            ganasteBronce = true;
        }
        if (puntuacionTotal >= puntajePlata && puntuacionTotal < puntajeOro && ganastePlata == false)
        {
            estrellaPlata.GetComponent<Animation>().Play();
            ganastePlata = true;
        }
        if (puntuacionTotal >= puntajeOro && ganasteOro == false)
        {
            estrellaOro.GetComponent<Animation>().Play();
            ganasteOro = true;
        }

    }

    public bool hasGanado()
    {
        if (ganasteOro)
            return true;
        else
            return false;
    }

}
