﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Global_SC_ActivadorPuerta : MonoBehaviour
{
    public Interactable interactable;
    public SteamVR_Action_Boolean gatillo;
    public Global_SC_ControlPuerta puerta;
    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (interactable.isHovering)
        {
            if (gatillo.GetLastStateUp(SteamVR_Input_Sources.Any)|| Input.GetKeyDown(KeyCode.Space))
            {
                puerta.AbrirPuerta();   
            }        

        }
    }
}
