﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Global_SC_GeneradorPuertas : MonoBehaviour
{
    // Start is called before the first frame update

    public int numPuertas, distancia;    
    public int orientacion;

    public float Velocidad;

    public bool diseñoManual;
    
    
    [SerializeField]
    public EscenaActiva escena;

    private float rotacion;    
    private int direccion;
    private int angulo;
    private int numPuerta=0;

    private float movimientoAngulo;

    private bool puedeRotar;

    private Global_SC_EfectoDesvanecer cambioEscenario;

    void Start()
    {
        
        cambioEscenario = gameObject.GetComponent<Global_SC_EfectoDesvanecer>();
        if (!diseñoManual)
        {
            generarPuertasDinamicamente();
        }
        else
        {
            angulo = 360 / numPuertas;
        }
        mostrarEscenarioCorespondiente();

    }

    // Update is called once per frame
    void Update()
    {      
        if (movimientoAngulo != rotacion)
        {
            
            transform.RotateAround(transform.position, (Vector3.down), direccion* Velocidad);
            
            movimientoAngulo += direccion * Velocidad;
            if (movimientoAngulo > 359)
            {
                movimientoAngulo = 0;
            }
            else if(movimientoAngulo<0)
            {
                movimientoAngulo = 359;
            }

        }
        else
        {
            puedeRotar = true;            
        }
    }

    public void RotarPuertas(int direccion)
    {
        if(puedeRotar){
            puedeRotar = false;
            numPuerta += direccion*1;
            if (numPuerta>=numPuertas)
            {
                numPuerta = 0;
            }
            else if(numPuerta < 0)
            {
                numPuerta = numPuertas-1;
            }
            if (escena == EscenaActiva.Inicio || escena == EscenaActiva.Fase)
            {
                    Debug.Log("Escenario es "+ numPuerta);
                    cambioEscenario.cambiarEscena(numPuerta);
                
                
            }           
            rotacion =  angulo* numPuerta;
            this.direccion = direccion;            
        }              
    }

    private void RotarPuerta(int puerta)
    {
        numPuerta = puerta;        
        rotacion = angulo * numPuerta;
        movimientoAngulo = rotacion;
        gameObject.transform.localRotation = Quaternion.Euler(0,-(angulo*puerta),0);

    }

    private void generarPuertasDinamicamente()
    {
        int activasP = getPuertasActivas();        
        numPuertas = Global_SC_Settings.getNumeroPuertas(escena)+1;
        angulo = 360 / (numPuertas);
        for (int i = 0; i < (numPuertas-1); i++)
        {
            GameObject puerta = Global_SC_Settings.getGOpuerta(escena, i);
            GameObject prefab;
            Global_SC_ControlPuerta cp;
            if (puerta != null)
            {
                prefab = Instantiate(puerta, gameObject.transform);
                cp = prefab.GetComponent<Global_SC_ControlPuerta>();
            }
            else
            {
                puerta = Global_SC_Settings.getGOpuerta(EscenaActiva.Dia, i);
                prefab = Instantiate(puerta, gameObject.transform);
                cp = prefab.GetComponent<Global_SC_ControlPuerta>();
                
                
            }            

            prefab.transform.localRotation = Quaternion.Euler(0, angulo * (i), 0);
            prefab.transform.localPosition = orientacion * prefab.transform.forward * distancia;
            
            cp.nombre = escena + " " + (i + 1);
            cp.valor = i + 1;
            cp.tipo = (int)escena;

            if (i >= activasP && activasP != 0)
            {
                cp.bloquearPuerta();
            }
            else
            {
                if (escena == EscenaActiva.Actividad)
                {
                    if (!Global_SC_Settings.actividadHabilitada(i))
                    {
                        cp.puertaEnConstruccion();
                    }
                    else
                    {
                   
                    }
                }
                else
                {
                    cp.activarPuerta();
                }
                

            }

        }
        crearPuertaExit();


        RotarPuerta(Global_SC_Settings.getPuertaDelante(escena));
        
    }
   
    private void crearPuertaExit()
    {
        GameObject prefab = Instantiate(Global_SC_Settings.getGOpuerta(EscenaActiva.Fase, 0), gameObject.transform);
        prefab.transform.localRotation = Quaternion.Euler(0, angulo * (numPuertas-1), 0);
        prefab.transform.localPosition = orientacion * prefab.transform.forward * distancia;
        Global_SC_ControlPuerta cp = prefab.GetComponent<Global_SC_ControlPuerta>();
        cp.nombre = "Exit";
        cp.valor = (int)escena-1;
        cp.tipo = -1;
        cp.estado = EstadosPuerta.Activa;
    }

    private int getPuertasActivas()
    {
        if (Global_SC_Settings.modoDios)
        {
            return 0;
        }
        else
        {
            Usuarios usuario = Global_SC_Settings.usuario_stc;
            if (escena == EscenaActiva.Fase)
            {
                return usuario.fase;
            }
            else
            if (escena == EscenaActiva.Dia)
            {
                if (usuario.fase == Global_SC_Settings.ubicacion_stc.fase)
                {
                    return usuario.dia;
                }
            }
            else
            if (escena == EscenaActiva.Actividad)
            {
                if (usuario.fase == Global_SC_Settings.ubicacion_stc.fase && usuario.dia == Global_SC_Settings.ubicacion_stc.dia)
                {
                    return usuario.actividad;
                }
            }
        }
        
        return 0;
    }

    private void mostrarEscenarioCorespondiente()
    {
        if (escena == EscenaActiva.Inicio)
        {
            Debug.Log("Escenario es " + numPuerta);
            cambioEscenario.cambiarEscena(numPuerta);
        }
        else
        {
            Instantiate(Global_SC_Settings.getEscenario(-1));
        }
    }
}
